<section class="hSlider">
  <h2 role="heading" aria-level="2" class="hidden"><?php pll_e('Découvrez nos produits'); ?></h2>
  <ul class="hSlider__list cS-hidden" id="hSlider">
    
    <?php
      $args_for_hSlider = ['post_type' => 'slider', 'posts_per_page' => 4];
      $loop_for_hSlider = new WP_Query( $args_for_hSlider );
    ;?>
    <?php while( $loop_for_hSlider->have_posts() ) : $loop_for_hSlider->the_post(); ?>
    <li class="hSlider__item" style="background-image: url('<?= get_the_post_thumbnail_url(get_the_ID(), 'hSlider') ;?>')">
      <section class="hSlider__wrapper">
				<div class="hSlider__container">
					<h3 role="heading" aria-level="3" class="hSlider__title"><?= the_title(); ?></h3>
					<p class="hSlider__excerpt">
						<?= str_limit( get_field('hSlider-desc'), 230 ) ;?>
					</p>
					<span class="hSlider__fake">
						<svg width="43" height="43" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<defs>
								<path id="b" d="M.9.4h39.2v38.9H.9z"/>
								<filter x="-3.8%" y="-3.9%" width="115.3%" height="115.4%" filterUnits="objectBoundingBox" id="a">
									<feOffset dx="3" dy="3" in="SourceAlpha" result="shadowOffsetOuter1"/>
									<feColorMatrix values="0 0 0 0 0.196078431 0 0 0 0 0.196078431 0 0 0 0 0.196078431 0 0 0 0.8 0" in="shadowOffsetOuter1"/>
								</filter>
							</defs>
							<g fill="none" fill-rule="evenodd">
								<use fill="#000" filter="url(#a)" xlink:href="#b"/>
								<use fill="#0072AD" xlink:href="#b"/>
								<path class="svg-more" d="M22 19h10v2H22v10h-2V21H10v-2h10V9h2v10z" fill="#FFF"/>
							</g>
						</svg>
						<span><?php pll_e('En savoir plus sur '); ?> <?= the_title(); ?></span>
					</span>
				</div>
				<a href="<?= get_field('hSlider-link'); ?>" class="hSlider__link"><span class="hidden"><?php pll_e('Vers '); ?> <?= the_title(); ?></span></a>
			</section>
    </li>
    <?php endwhile; ?>
		<?php wp_reset_postdata(); ?>
  </ul>
	<div class="hSlider__buttons-wrapper">
		<div class="hSlider__buttons">
			<button class="hSlider__button hSlider__button--left">
				<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg">
					<g fill="none" fill-rule="evenodd">
						<rect fill-opacity=".6" fill="#181818" width="50" height="50" rx="25"/>
						<path d="M19.07 25.435l11.4-11.399L29.434 13 17 25.434l.001.001-.001.001L29.434 37.87l1.036-1.036-11.4-11.399z" fill="#FFF"/>
					</g>
				</svg>
			</button>
			<button class="hSlider__button hSlider__button--right">
				<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg">
					<g fill="none" fill-rule="evenodd">
						<rect fill-opacity=".6" fill="#181818" width="50" height="50" rx="25"/>
						<path d="M31.4 25.4L20 14l1-1 12.5 12.4L21 38l-1-1 11.4-11.5z" fill="#FFF"/>
					</g>
				</svg>
			</button>
		</div>
	</div>
</section>
