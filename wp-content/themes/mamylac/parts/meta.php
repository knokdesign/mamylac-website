<?php $fields =  get_fields(); ?>

<!DOCTYPE html>
<html lang="fr">
<head>
	
	
	<?php if (is_page('accueil')): ?>
		<title><?= get_bloginfo()  . ' - ' . get_bloginfo('description'); ?></title>
	<?php else: ?>
		<title><?=  strip_tags(get_the_title()); ?> - <?= get_bloginfo() ;?></title>
	<?php endif; ?>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="<?= get_field('meta-description') ;?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php theme_asset('/css/styles.css') ;?>" type="text/css">
	
	<!-- Twitter Card data -->
	<!-- <meta name="twitter:card" content="summary"> -->
	<!-- <meta name="twitter:site" content="@publisher_handle"> -->
	<meta name="twitter:title" content="<?= the_title(); ?>">
	<meta name="twitter:description" content="<?= get_field('meta-description') ;?>">
	<!-- <meta name="twitter:creator" content="@author_handle"> -->
	<!-- Twitter Summary card images must be at least 120x120px -->
	<!-- <meta name="twitter:image" content="http://www.example.com/image.jpg"> -->
	
	<!-- Open Graph data -->
	<meta property="og:title" content="<?= the_title() ;?>" />
	<!-- <meta property="og:type" content="article" /> -->
	<meta property="og:url" content="<?= the_permalink(); ?>" />
	<!-- <meta property="og:image" content="http://example.com/image.jpg" /> -->
	<meta property="og:description" content="<?= get_field('meta-description') ;?>" />
	<meta property="og:site_name" content="<?= get_bloginfo(); ?>" />
	<!-- <meta property="fb:admins" content="Facebook numeric ID" /> -->
	
	<link rel="apple-touch-icon" sizes="180x180" href="<?= theme_asset('favicon/apple-touch-icon.png') ;?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= theme_asset('favicon/favicon-32x32.png') ;?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= theme_asset('favicon/favicon-16x16.png') ;?>">
	<link rel="manifest" href="<?= theme_asset('favicon/site.webmanifest') ;?>">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="theme-color" content="#ffffff">
 
	<?php wp_head(); ?>
	
	<!--[if lt IE 9]>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<![endif]-->
	
	
</head>

