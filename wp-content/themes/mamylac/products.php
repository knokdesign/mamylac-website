<?php

/*
Template Name: Produits
*/

get_template_part('parts/meta');; ?>

<body class="products" data-page="products">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?= pll_current_language() == 'fr' ? the_permalink(348) : the_permalink(699);?>"><?php pll_e('Retour à la liste des produits'); ?></a>
		</div>
	</div>
	
  <section class="containerGlobal containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
    
    <?php
    global $post;
    $post_slug = $post->post_name;
    
    $args_for_posts = [ 'post_type' => 'produits', 'category_name' => $post_slug ];
    $loop_for_posts = new WP_Query( $args_for_posts );
    
    ;?>
    
    
    <div class="products__wrapper">
      <?php while( $loop_for_posts->have_posts() ) : $loop_for_posts->the_post(); ?>
				
      <section class="products__item">
        <div class="products__img">
  
          <?php
          $img        = get_field('product_image');
          $img_thumb  = 'productImage2';
          ?>
          
          <img src="<?= $img['sizes'][$img_thumb]; ?>" width="287" height="347" alt="<?= $img['alt']; ?>">

        </div>
        <div class="products__fake-link">
          <h3 class="products__title"><?= the_title(); ?></h3>
        </div>
				
				<a class="products__link" href="<?= the_permalink(); ?>" ><span class="hidden"><?php pll_e('Voir le produit '); the_title(); ?></span></a>
				
      </section>
      
      <?php endwhile; ?>
      
      <?php wp_reset_postdata(); ?>
    
    </div>
		
</main>

<?php get_footer(); ?>

