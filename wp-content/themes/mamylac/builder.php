<?php

/*
Template Name: Builder
*/

get_template_part('parts/meta');

$listTitleLeft = the_sub_field('list_title_left');
$listTitleRight = the_sub_field('list_title_right');

?>

<body class="machine" data-page="machine">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  <div class="breadcrumb containerGlobal">
    <div class="breadcrumb__link">
      <svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
        <path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
      </svg>
    </div>
  </div>
  <div class="containerGlobal containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
    <span class="subtitle"><?= get_field('subtitle'); ?></span>
  </div>
  <?php while (the_flexible_field('flexible')): ?>
    <div class="containerGlobal single-post">
      
      
      <?php if (get_row_layout() == 'Wysiwig'): ?>
        
        <div class="article__txt">
          <?php echo the_sub_field('wysiwyg'); ?>
        </div>
      
      <?php endif; ?>
      
      <?php if (get_row_layout() == 'Titre'): ?>
        
        <h3 class="title24Bold">
          <?php echo the_sub_field('title'); ?>
        </h3>
      
      <?php endif; ?>
      
      <?php if (get_row_layout() == 'deux_listes_cote_a_cote'): ?>
        <div class="list-col-wrapper">
          <div class="list-col list-left">
            <?php if (get_sub_field('list_title_left') !== null ) : ?>
              <h3 role="heading" aria-level="3" class="list-title list-title-left"><?= the_sub_field('list_title_left'); ?></h3>
            <?php endif; ?>
            <ul>
              <?php if( have_rows('list_left') ): while ( have_rows('list_left') ) : the_row(); ?>
                <li><?= the_sub_field('item'); ?></li>
              <?php endwhile; endif; ?>
            </ul>
          </div>
          <div class="list-col list-right">
            <?php if (get_sub_field('list_title_right') !== null ) : ?>
              <h3 role="heading" aria-level="3" class="list-title list-title-right"><?= the_sub_field('list_title_right'); ?></h3>
            <?php endif; ?>
            <ul>
              <?php if( have_rows('list_right') ): while ( have_rows('list_right') ) : the_row(); ?>
                <li><?= the_sub_field('item'); ?></li>
              <?php endwhile; endif; ?>
            </ul>
          </div>
        </div>
      <?php endif; ?>
      
      
      
      <?php if (get_row_layout() == 'bloc_contacter_mamylac'): ?>
        
        <?php
        $img3        = get_sub_field('image');
        $img3_thumb  = 'productImage';
        $img3_width  = $img_thumb . '-width';
        $img3_height = $img_thumb . '-height';
        ?>
        
        <div class="contactBlock">
          <div class="contactBlock__col contactBlock__col--left">
            <img src="<?= $img3['sizes'][$img3_thumb]; ?>" width="<?= $img3['width']; ?>" height="<?= $img3['height']; ?>" alt="<?= $img3['alt']; ?>">
          </div>
          <div class="contactBlock__col contactBlock__col--right">
            <h3 role="heading" aria-level="3" class="contactBlock__title"><?= the_sub_field('title_haut') ;?></h3>
            <p class="contactBlock__txt"><?= the_sub_field('txt_haut') ;?></p>
            <strong><?php pll_e('Envie de tester'); ?> <?= the_sub_field('machine_name'); ?>&nbsp;? <?php pll_e('Possibilité de location'); ?></strong>
            <a href="<?= pll_current_language() == 'fr' ? the_permalink(204) : the_permalink(728);?>" class="contactBlock__link cta"><span><?php pll_e('Contacter Mamylac'); ?></span></a>
          </div>
        </div>
      
      <?php endif; ?>
      
      <?php if (get_row_layout() == 'Image'): ?>
        <div class="machine__img">
          <?php
          $img        = get_sub_field('image');
          $img_thumb  = 'wysiwygImage';
          $img_width  = $img_thumb . '-width';
          $img_height = $img_thumb . '-height';
          ?>
          
          <img src="<?= $img['sizes'][$img_thumb]; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>" alt="<?= $img['alt']; ?>">
        
        </div>
      <?php endif; ?>
    
    
    
    
    </div>
    
    <?php if (get_row_layout() == 'bloc_sur_fond_bleu'): ?>
      
      <?php
      $img2        = get_sub_field('img');
      $img2_thumb  = 'wysiwygImage';
      $img2_width  = $img_thumb . '-width';
      $img2_height = $img_thumb . '-height';
      ?>
      
      <section class="blueback">
        <div class="containerGlobal">
          <h3 class="blueback__title"><?php echo the_sub_field('title'); ?></h3>
          <div class="blueback__text"><?php echo the_sub_field('text'); ?></div>
          <img src="<?= $img2['sizes'][$img2_thumb]; ?>" width="<?= $img2['width']; ?>" height="<?= $img2['height']; ?>" alt="<?= $img2['alt']; ?>">
        </div>
      </section>
    
    <?php endif; ?>
  
  <?php endwhile; ?>

</main>

<?php get_footer(); ?>

