<?php
/*
Template Name: About
*/
get_template_part('parts/meta');; ?>

<body class="about" data-page="about">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	<div class="containerGlobal about__container containerGlobalFirst">
		<h2 role="heading" aria-level="2" class="about__title title24Bold"><?php the_title(); ?></h2>
    
    <?php while (the_flexible_field('about')): ?>
      
      <?php if (get_row_layout() == 'Ajoutez du texte'): ?>
				
			<?php echo the_sub_field('about-wysiwyg'); ?>
      
      <?php elseif (get_row_layout() == 'Ajoutez une image'): ?>
				
				<figure>
          <?php
          $img        = get_sub_field('about-image');
          $img_thumb  = 'wysiwygImage';
          $img_width  = $img_thumb . '-width';
          $img_height = $img_thumb . '-height';
          ?>
					
					<img src="<?= $img['sizes'][$img_thumb]; ?>" width="<?= $img['sizes'][$img_width]; ?>" height="<?= $img['sizes'][$img_height]; ?>" alt="<?= $img['alt']; ?>">
				
				</figure>
      
      <?php endif; ?>
    
    <?php endwhile; ?>
		
		<a href="<?= pll_current_language() == 'fr' ? the_permalink(204) : the_permalink(728);?>" class="cta">
			<span><?php pll_e('Contacter Mamylac'); ?></span>
		</a>
	
	
	</div>
	
	<blockquote class="about__blockquote">
		<strong><?php pll_e('50 années'); ?></strong>
		<span><?php pll_e('au service de l’élevage de veau'); ?></span>
	</blockquote>
	
	<div class="containerGlobal">
		
		<div class="history__wrapper">
      
      <?php
      
      // check if the repeater field has rows of data
      if( have_rows('history') ):
        
        // loop through the rows of data
        while ( have_rows('history') ) : the_row(); ?>
	
					<div class="flip-container" ontouchstart="this.classList.toggle('hover');">
						<div class="flipper">
							<div class="front">
								<p><?= the_sub_field('history_date'); ?></p>
							</div>
							<div class="back" style="background-image: url('<?= get_sub_field('history_image')['sizes']['aboutImage'];?>')">
								<p><?= the_sub_field('history_texte'); ?></p>
							</div>
						</div>
					</div>
        
			<?php endwhile; endif; ?>
			
			
		</div>
	</div>
	
</main>

<?php get_footer(); ?>

