<?php

/*
Template Name: Contact
*/

get_template_part('parts/meta');; ?>

<body class="contact" data-page="contact">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  <div class="containerGlobal contact__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="about__title title24Bold"><?php the_title(); ?></h2>
		
    <div class="contact__container2">
			<div class="contact__wrapper contact__wrapper--left">
				
				<p class="contact__intro"><?= get_field('intro'); ?></p>
				<strong class="about__title2 title24Bold"><?= get_field('titre_2'); ?></strong>
				<p class="contact__intro"><?= get_field('content_2'); ?></p>
				<ul class="contact__list">
					<li class="contact__item">
						<svg width="38" height="38" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient x1="0%" y1="50%" y2="50%" id="a">
									<stop stop-color="#53B8E9" offset="0%"/>
									<stop stop-color="#2B69AF" offset="100%"/>
								</linearGradient>
							</defs>
							<g transform="translate(-149 -592)" fill="url(#a)">
								<path d="M158 613.4l-.8 9a1 1 0 0 0 2 .1l.8-9a1 1 0 0 0-2-.1zm18 .1l.8 9a1 1 0 0 0 2-.1l-.8-9a1 1 0 1 0-2 .1zm-9 8.1v7.4a1 1 0 0 0 2 0v-7.4a1 1 0 0 0-2 0z"/>
								<path d="M184.6 626.6l-6.2-5a1 1 0 0 0-1.1 0l-9.3 6.2-9.3-6.2a1 1 0 0 0-1.1 0l-6.2 5 2-11.9 4-4.7a1 1 0 0 0-1.6-1.3l-4.1 5a1 1 0 0 0-.2.4l-2.5 14.7a1 1 0 0 0 1.6 1l7.6-6.1 9.2 6.1c.4.3.8.3 1.2 0l9.2-6.1 7.6 6a1 1 0 0 0 1.6-.9l-2.5-14.7a1 1 0 0 0-.2-.5l-4-4.9a1 1 0 1 0-1.6 1.3l3.9 4.7 2 11.9z"/>
								<path d="M168.9 616.3l3.4-3.4c3-3.4 4.7-6.8 4.7-9.9a9 9 0 1 0-18 0c0 3.1 1.8 6.5 4.7 10a38 38 0 0 0 4.3 4l.9-.7zM179 603c0 3.7-2 7.5-5.2 11.2a40.3 40.3 0 0 1-5.2 5 1 1 0 0 1-1.2 0 18.8 18.8 0 0 1-1.6-1.4 40 40 0 0 1-3.6-3.6c-3.2-3.7-5.2-7.5-5.2-11.2a11 11 0 1 1 22 0z"/>
								<path d="M168 608.2a5.1 5.1 0 1 1 0-10.3 5.1 5.1 0 0 1 0 10.3zm0-2a3.1 3.1 0 1 0 0-6.3 3.1 3.1 0 0 0 0 6.3z"/>
							</g>
						</svg>
						<address><?= get_field('op_address', 'option') ;?></address>
					</li>
					<li class="contact__item">
						<svg class="svg-tel" width="21" height="32" xmlns="http://www.w3.org/2000/svg">
								<defs>
									<linearGradient x1="0%" y1="50%" y2="50%" id="b">
										<stop stop-color="#53B8E9" offset="0%"/>
										<stop stop-color="#2B69AF" offset="100%"/>
									</linearGradient>
								</defs>
								<g transform="translate(-158 -656)" fill="url(#b)">
									<path d="M159.6 662h18.8c.3 0 .6-.2.6-.5s-.3-.5-.6-.5h-18.8c-.3 0-.6.2-.6.5s.3.5.6.5zm18.8 18h-18.8c-.3 0-.6.2-.6.5s.3.5.6.5h18.8c.3 0 .6-.2.6-.5s-.3-.5-.6-.5z"/>
									<path d="M175.7 686.7c1.1 0 2-1 2-2v-25.4a2 2 0 0 0-2-2h-14.4a2 2 0 0 0-2 2v25.4c0 1 .9 2 2 2h14.4zm0 1.3h-14.4a3.3 3.3 0 0 1-3.3-3.3v-25.4c0-1.8 1.5-3.3 3.3-3.3h14.4c1.8 0 3.3 1.5 3.3 3.3v25.4c0 1.8-1.5 3.3-3.3 3.3z"/>
									<path d="M168.5 686a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z"/>
								</g>
							</svg>
						<a href="tel:<?= str_replace(' ', '', get_field('op_tel', 'option') ) ;?>">Tel&nbsp;: <?= get_field('op_tel', 'option') ;?></a>
					</li>
					<li class="contact__item">
						<svg width="36" height="36" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient x1="0%" y1="50%" y2="50%" id="c">
									<stop stop-color="#53B8E9" offset="0%"/>
									<stop stop-color="#2B69AF" offset="100%"/>
								</linearGradient>
							</defs>
							<g transform="translate(-150 -713)" fill="url(#c)">
								<path d="M159 747.5h18v-9h-18v9zm18 1.5h-18c-.8 0-1.5-.7-1.5-1.5v-9.8c0-.4.3-.7.8-.7h19.4c.5 0 .8.3.8.8v9.7c0 .8-.7 1.5-1.5 1.5z"/>
								<path d="M151.5 728c0-.8.7-1.5 1.5-1.5h30c.8 0 1.5.7 1.5 1.5v12.8h-6v1.5h6.8c.4 0 .7-.4.7-.8V728a3 3 0 0 0-3-3h-30a3 3 0 0 0-3 3v13.5c0 .4.3.8.8.8h6.7v-1.5h-6V728zm26.3-5.2h2.2c.4 0 .8.3.8.7v1.5h1.4v-1.5c0-1.2-1-2.3-2.2-2.3h-2.3v1.5z"/>
								<path d="M162.8 742.3h10.4v-1.5h-10.4zm0 3h10.4v-1.5h-10.4zm-7.5-20.3v-1.5c0-.4.3-.8.7-.8h2.3v-1.5H156c-1.2 0-2.3 1-2.3 2.3v1.5h1.6zm.7 13.5h24V737h-24zm-1.5-8.2v-1.5h4.5v1.5h-4.5zm16.5-16.5h1.5v6h5.3v1.5h-5.3c-.8 0-1.5-.7-1.5-1.5v-6z"/>
								<path d="M171.4 714.5l5.6 6.3v5h1.5v-5.3c0-.2 0-.4-.2-.5l-6-6.7a.8.8 0 0 0-.5-.3H159c-.8 0-1.5.7-1.5 1.5v11.3h1.5v-11.3h12.4z"/>
							</g>
						</svg>
						<a href="tel:<?= str_replace(' ', '', get_field('op_fax', 'option') ) ;?>">Fax&nbsp;: <?= get_field('op_fax', 'option') ;?></a>
					</li>
					<?php if(get_field('op_email', 'option')): ?>
					<li class="contact__item">
						<svg width="36" height="29" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient x1="0%" y1="50%" y2="50%" id="d">
									<stop stop-color="#53B8E9" offset="0%"/>
									<stop stop-color="#2B69AF" offset="100%"/>
								</linearGradient>
							</defs>
							<g transform="translate(-150 -774)" fill="url(#d)">
								<path d="M183.5 801c.5 0 .9-.4.9-.8v-23.7c0-.5-.4-.9-.9-.9h-31c-.5 0-.9.4-.9.9v23.7c0 .4.4.8.9.8h31zm0 1.6h-31a2.5 2.5 0 0 1-2.5-2.4v-23.7c0-1.4 1.1-2.5 2.5-2.5h31c1.4 0 2.5 1.1 2.5 2.5v23.7c0 1.3-1.1 2.4-2.5 2.4z"/>
								<path d="M170.5 789.9l-.3.2a3.2 3.2 0 0 1-4.5 0l-.2-.2-14.1 12.5-1.1-1.2 14-12.4-14-13.4 1-1.2 15.5 14.7c.7.7 1.7.7 2.3 0l15.5-14.7 1.1 1.2-14 13.3 14 12.5-1 1.2-14.2-12.5z"/>
							</g>
						</svg>
						<a href="mailto:<?= get_field('op_email', 'option') ;?>">Email&nbsp;: <?= get_field('op_email', 'option') ;?></a>
					</li>
					<?php endif; ?>
					<li class="contact__item">
						<svg width="36" height="36" xmlns="http://www.w3.org/2000/svg">
							<defs>
								<linearGradient x1="0%" y1="50%" y2="50%" id="e">
									<stop stop-color="#53B8E9" offset="0%"/>
									<stop stop-color="#2B69AF" offset="100%"/>
								</linearGradient>
							</defs>
							<path d="M18.8 21.4V36H6.5C3.1 36 0 33 0 29.5v-23C0 3.1 3 0 6.5 0h23C32.9 0 36 3 36 6.5v23c0 3.4-3 6.5-6.5 6.5h-7V21.4h5.8v-4.1h-5.7v-3.1c0-1.3.8-2.3 1.9-2.3h3.8V7.7h-3.8c-3 0-5.7 3-5.7 6.5v3h-4.2v4.2h4.2zm10.7 13c2.6 0 5-2.3 5-5V6.6c0-2.6-2.4-5-5-5h-23c-2.6 0-5 2.4-5 5v23c0 2.6 2.4 5 5 5h10.7V23H13v-7.3h4.2v-1.5c0-4.3 3.3-8 7.3-8h5.4v7.2h-5.4c-.1 0-.4.3-.4.8v1.5H30V23h-6v11.4h5.4z" fill="url(#e)"/>
						</svg>
						<a target="_blank" href="<?= get_field('op_facebook', 'option'); ?>"><?php pll_e('Suivez-nous sur Facebook'); ?></a>
					</li>
				</ul>
			</div>
			
			<div class="contact__wrapper contact__wrapper--right">
				<?php
					if (pll_current_language() == 'fr' ) {
						echo do_shortcode('[contact-form-7 id="928" title="Formulaire de contact FR" html_class="form"]');
					} elseif (pll_current_language() == 'nl' ) {
						echo do_shortcode('[contact-form-7 id="929" title="Formulaire de contact NL" html_class="form"]');
					} elseif (pll_current_language() == 'en' ) {
            do_shortcode('[contact-form-7 id="1202" html_class="form"]');
					}
				?>
			</div>

		</div>
		
		<div class="persons">
			<?php if( have_rows('persons') ): while ( have_rows('persons') ) : the_row(); ?>
				<figure class="person">
          <?php $img = get_sub_field('photo'); ?>
					<img class="person__img" src="<?= $img['sizes']['contactPhoto']; ?>" width="345" height="305" alt="">
					<figcaption class="person__figcaption">
						<strong class="person__name"><?= get_sub_field('name') ;?></strong>
						<span class="person__role"><?= get_sub_field('role') ;?></span>
						<a href="tel:<?= str_replace(' ', '', the_sub_field('phone')); ?>" class="person__link">
							<svg width="20" height="16" xmlns="http://www.w3.org/2000/svg">
								<path d="M17.3 10.3c-.1-.7-.7-1.2-1.3-1.4-2.6-.6-3.2-2-3.3-3.7A15 15 0 0 0 10 5a15 15 0 0 0-2.7.2c0 1.6-.7 3.1-3.3 3.7-.6.2-1.2.7-1.3 1.4l-.5 3.4a2 2 0 0 0 2 2.3h11.6a2 2 0 0 0 2-2.3l-.5-3.4zM10 13.5A2.5 2.5 0 0 1 7.5 11c0-1.4 1.1-2.5 2.5-2.5s2.5 1.1 2.5 2.5-1.1 2.5-2.5 2.5zM20 4c0-1.5-3.9-4-10-4S0 2.5 0 4s0 3.5 2.6 3.1c3-.4 2.7-1.4 2.7-2.8C5.3 3.3 7.7 3 10 3c2.3 0 4.7.2 4.7 1.3 0 1.4-.2 2.4 2.7 2.8C20 7.5 20 5.5 20 4z" fill="#91268F"/>
							</svg>
							<span><?= the_sub_field('phone'); ?></span>
						</a>
						<a href="mailto:<?= the_sub_field('email'); ?>" class="person__link person__link--mail">
							<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">
								<path d="M14.6 12.2c0 .8.2 1.1.9 1.1 1.4 0 2.3-1.7 2.3-4.7 0-4.5-3.3-6.7-7.4-6.7C6 2 2.3 4.8 2.3 10.1c0 5.1 3.4 8 8.5 8 1.8 0 3-.3 4.7-.9l.4 1.6c-1.7.6-3.6.8-5.1.8C4 19.6.4 15.9.4 10S4.6.4 10.4.4c6 0 9.2 3.6 9.2 8 0 3.8-1.2 6.6-4.9 6.6-1.7 0-2.8-.6-2.9-2.1-.4 1.6-1.6 2.1-3.1 2.1-2.1 0-3.9-1.6-3.9-4.8 0-3.3 1.6-5.3 4.3-5.3 1.5 0 2.4.6 2.8 1.5l.7-1.3h2v7zm-3-3.2c0-1.3-1-1.9-1.7-1.9-1 0-2 .8-2 2.9 0 1.6.8 2.6 2 2.6.7 0 1.8-.5 1.8-2V9z" fill="#91268F"/>
							</svg>
							<span><?= the_sub_field('email'); ?></span>
						</a>
					</figcaption>
				</figure>
			<?php endwhile; endif; ?>
		</div>
  </div>
	
	<div class="map__wrapper">
		<div id="map"></div>
	</div>


</main>

<?php get_footer(); ?>

