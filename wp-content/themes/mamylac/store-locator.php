<?php

/*
Template Name: Store locator
*/

get_template_part('parts/meta');; ?>

<body class="page-link" data-page="page-link">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  <div class="containerGlobal containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
    
    <?= do_shortcode('[wpsl template="default" map_type="roadmap" auto_locate="true" start_marker="green" store_marker="blue"]'); ?>
  
  </div>


</main>

<?php get_footer(); ?>

