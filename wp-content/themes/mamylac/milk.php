<?php
/*
Template Name: Poudre de lait
*/
get_template_part('parts/meta');; ?>

<body class="milk" data-page="milk">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?= pll_current_language() == 'fr' ? the_permalink(340) : the_permalink(707);?>"><?php pll_e('Retour'); ?></a>
		</div>
	</div>
	
  <div class="containerGlobal milk__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
  
    <?php while (the_flexible_field('milk_content')): ?>
    
      <?php if (get_row_layout() == 'Texte à gauche - Image à droite'): ?>
        
        <div class="txt-img">
          
            <div class="txt-img__txt">
              <?php echo the_sub_field('texte'); ?>
            </div>
          
            <div class="txt-img__img">
            <?php
              $img        = get_sub_field('Image');
              $img_thumb  = 'wysiwygImage';
              $img_width  = $img_thumb . '-width';
              $img_height = $img_thumb . '-height';
            ?>
    
            <img src="<?= $img['sizes'][$img_thumb]; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>" alt="<?= $img['alt']; ?>">
          </div>
          
        </div>
        
    
      <!-- SUBTITLE -->
      <?php elseif (get_row_layout() == 'Sous-titre'): ?>
        <span class="subtitle"><?php echo the_sub_field('sous-titre'); ?></span>
    
      <!-- PARAGRAPHE -->
      <?php elseif (get_row_layout() == 'Paragraphe'): ?>
        <div class="containerParagraph">
          <?php echo the_sub_field('Paragraphe'); ?>
        </div>
  
      <!-- Multiple bloc horizontal -->
      <?php elseif (get_row_layout() == 'Multiple bloc horizontal'): ?>
        <span class="blocHorizontalTitle"><?php echo the_sub_field('blocHorizontalTitle'); ?></span>
  
        <div class="blocHorizontalContainer">
        <?php if( have_rows('bloc_de_texte') ): while ( have_rows('bloc_de_texte') ) : the_row(); ?>
          <div class="blocHorizontalWrapper">
            <span class="blocHorizontalSubtitle"><?= the_sub_field('sous-titre'); ?></span>
            <div class="blocHorizontalTexte"><?= the_sub_field('texte'); ?></div>
          </div>
        <?php endwhile; endif; ?>
        </div>
  
      <?php elseif (get_row_layout() == 'Multiple liste vertical'): ?>
        <span class="blocHorizontalTitle"><?php echo the_sub_field('blocHorizontalTitle'); ?></span>
    
        <div class="blocHorizontalContainer">
          <?php if( have_rows('liste') ): while ( have_rows('liste') ) : the_row(); ?>
            <ul class="blocVerticalWrapper">
              <li class="blocVerticalTexte"><?= the_sub_field('texte'); ?></li>
            </ul>
          <?php endwhile; endif; ?>
        </div>
      
      
      <?php endif; ?>
      
    <?php endwhile; ?>
    
  </div>
</main>

<?php get_footer(); ?>

