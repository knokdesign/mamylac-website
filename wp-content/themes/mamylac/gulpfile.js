const gulp = require('gulp'),
    elixir = require( 'laravel-elixir' ),
    clean = require( 'gulp-dest-clean' ),
    plumber = require( 'gulp-plumber'),
    iconfont = require( 'gulp-iconfont' ),
    runTimestamp = Math.round( Date.now() / 1000 );

require( 'laravel-elixir-config' );
require( 'laravel-elixir-pug' );
require( 'laravel-elixir-tinypng' );


elixir(function (mix) {
  // Copy and watch on fonts folder
  mix.copy( elixir.config.copy.fonts.src, elixir.config.copy.fonts.dest );
  mix.task( 'watch_fonts', elixir.config.copy.fonts.watch );
  mix.task( 'watch_pug', elixir.config.pug.watch );


  // TinyPNG
  mix.tinypng({
    tinypngKey: '6R3ddSeN039Qn91I3YBCUN2j7XjAV-7u',
    sigFile: '.tinypng-sigs',
    summarize: true,
    folder: elixir.config.img.folder,
    outputFolder:  elixir.config.img.outputFolder,
  });

  // Stylus
  mix.stylus( 
    elixir.config.stylus.files.src, 
    elixir.config.stylus.files.dest 
  );

  mix.scripts([
    elixir.config.js.vendor,
    elixir.config.js.base,
    elixir.config.js.class,
    elixir.config.js.pages,
  ]);


  mix.browserSync({
    // TODO: Put good proxy here
    proxy: 'mamylac.test'
  });
})


// Watch on fonts folder
gulp.task('watch_fonts', function () {
  gulp.src( elixir.config.copy.fonts.src )
    .pipe( clean( elixir.config.copy.fonts.dest ) )
    .pipe( gulp.dest( elixir.config.copy.fonts.dest ) )
});

gulp.task('watch_pug', function () {
  gulp.src( elixir.config.pug.files.src )
      .pipe( gulp.dest( elixir.config.pug.files.dest ) )
});

// Generate iconfont
gulp.task('iconfont', function() {
  return gulp.src( elixir.config.iconfont.src )
    .pipe( iconfont( {
      fontName: 'iconfont',
      prependUnicode: true,
      formats: [ 'ttf', 'eot', 'woff', 'svg' ],
      timestamp: runTimestamp,
      normalize: true,
      fontHeight: 1001
    } ) )
    .pipe( gulp.dest ( elixir.config.iconfont.dest ) );
})
