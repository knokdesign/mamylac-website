<?php
/*
Template Name: Poudre de lait
*/
get_template_part('parts/meta');; ?>

<body class="milk" data-page="milk">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>

<div class="hHeader">
  <div class="nav-wrapper">
    <div class="containerGlobal">
      <nav class="nav-main" id="main-nav">
        <h2 role="heading" aria-level="1" class="hidden"><?php pll_e('Navigation principale'); ?></h2>
        <a href="<?= get_home_url(); ?>" class="nav-main__home" title="<?php pll_e('Vers la page d’accueil'); ?>">
          <span class="hidden"><?php pll_e('Vers la page d’accueil'); ?></span>
          <svg width="202" height="91" xmlns="http://www.w3.org/2000/svg" class="main-nav__svg">
            <g fill="none" fill-rule="evenodd">
              <path fill="#F7F7F7" d="M0 0h202v91H0z" class="nav-main__svg--background"/>
              <g fill="#0072AD">
                <path d="M139.1 48.3l-3 4h-13.7V36.8h4.7v11.5zm7.5 4l2.9-4h6.4l-4-5.6-7 9.6H139l10-13.7a4.3 4.3 0 0 1 3-1.8c.6 0 1.2.2 1.8.6.5.3 1 .7 1.3 1.2l9.7 13.7h-18.2zm-122.1 0l2-8.1 4.4 7c.3.3.6.7 1.1 1l1.5.3c1 0 1.8-.5 2.4-1.4l4.3-7 2 8.2h5.3l-4-13.7c-.2-.5-.5-1-1-1.4-.5-.4-1.1-.6-1.8-.6-1 0-1.9.6-2.6 1.8l-4.7 7.7-4.7-7.7c-.8-1.2-1.7-1.8-2.7-1.8a2.8 2.8 0 0 0-2.7 2l-4 13.7h5.2zm157.6-4h-9.6a5 5 0 0 1-3-1 3.3 3.3 0 0 1-1.3-2.7c0-1 .4-2 1.3-2.7.8-.7 2-1 3.2-1h7.2l-1.1-4h-6.5c-2.6 0-4.6.7-6.3 2a6.7 6.7 0 0 0-2.6 5.5 7 7 0 0 0 2.7 5.7c1.1 1 2.5 1.6 4 2l2.6.2h10.5l-1.1-4zM65 48.3h-6.5l-3 4h18.2l-9.6-13.7c-.4-.5-.8-1-1.3-1.2-.6-.4-1.2-.6-1.8-.6-.6 0-1.2.2-1.8.6-.5.3-1 .7-1.3 1.2L41.1 61.8h6l13.8-19 4 5.5zm30.7-11.7c-1 0-1.9.7-2.6 1.8l-4.7 7.7-4.8-7.7c-.7-1.1-1.6-1.8-2.6-1.8a2.8 2.8 0 0 0-2.7 2l-4 13.7h5.2l2-8.1 4.4 7 1 1c.6.2 1 .3 1.6.3 1 0 1.8-.5 2.3-1.4l4.4-7 2 8.2h5.3l-4-13.7c-.2-.5-.5-1-1-1.4-.5-.4-1.1-.6-1.8-.6"/>
                <path d="M110.9 43.1l-5.8-6.3h-6.2l9.5 10.4v5.1h4.8v-5.1l9.2-10.4h-6.1z"/>
              </g>
            </g>
          </svg>
        </a>
        <button class="hamburger hamburger--arrow-r" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <div class="nav-menu__wrapper">
          <ul class="nav-menu">
            
        
            <?php foreach(b_get_nav_items('main-nav') as $item): ?>
          
              <li class="nav-item">
                <a href="<?= $item->url; ?>" class=""><?= $item->label; ?></a>
            
                <?php if($item->children): ?>
                  <div class="sub-nav">
                    <ul class="sub-nav-group">
                      <?php foreach ($item->children as $sub): ?>
                        <li><a href="<?= $sub->url; ?>"><?= $sub->label; ?></a></li>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                <?php endif; ?>
          
              </li>
        
            <?php endforeach; ?>
      
          </ul>
        </div>
  
      </nav>
  
      <ul class="nav-lang">
        <?php pll_the_languages(); ?>
      </ul>

    </div>
  </div>
  <div class="sHeader" style="background-image: url('<?php theme_asset('img/error404.jpg'); ?>')">
  </div>

<main>
  
  <div class="breadcrumb containerGlobal">
    <div class="breadcrumb__link">
      <svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
        <path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
      </svg>
      <a href="<?= home_url() ?>"><?php pll_e('Retour à la page d’accueil'); ?></a>
    </div>
  </div>
  
  <div class="containerGlobal milk__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php pll_e('Cette page n’existe pas ou a été supprimée !'); ?></h2>

  
  </div>
</main>

<?php get_footer(); ?>

