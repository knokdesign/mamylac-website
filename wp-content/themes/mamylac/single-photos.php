<?php

get_template_part('parts/meta');; ?>

<body class="album" data-page="album">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  <div class="containerGlobal about__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="about__title title24Bold"><?php the_title(); ?></h2>
  	
    <div class="gallery-post">
      <?php $images = get_field('album') ;?>
      <?php if( $images ): ?>
        <div class="lightGallery" id="galleryPost">
          <?php foreach ($images as $image ): ?>
            <a class="lightGallery__link" href="<?= $image['url'];?>" data-sub-html=".lightGallery__caption">
							<div class="lightGallery__wrapper">
								<img class="lightGallery__img" src="<?= $image['sizes']['albumCover'];?>" width="267" height="211" alt="">
								<div class="lightGallery__caption">
									<p class="lightGallery__p"><?= $image['caption'];?></p>
								</div>
								<svg width="30" height="30" xmlns="http://www.w3.org/2000/svg">
									<path d="M29.3 25.4l-7.1-7.2c1-1.8 1.7-3.9 1.7-6C23.9 5.5 18.2 0 11.7 0 5.3 0 0 5.3 0 11.7 0 18.2 5.6 24 12.1 24c2.2 0 4.2-.6 6-1.6l7.1 7.2c.7.7 1.9.7 2.6 0l1.8-1.8c.7-.7.4-1.6-.3-2.3zM3.6 11.7c0-4.4 3.7-8 8.1-8 4.5 0 8.5 4 8.5 8.4a8 8 0 0 1-8 8.1c-4.6 0-8.6-4-8.6-8.5z" fill="#FFF"/>
								</svg>
							</div>
            </a>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
	</div>

</main>

<?php get_footer(); ?>

