<?php

/*
Template Name: Conseils
*/

get_template_part('parts/meta');; ?>

<body class="post" data-page="post">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  <div class="containerGlobal containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
    
    <?php
    $args_for_posts = [ 'post_type' => 'conseils', 'posts_per_page' => '9', 'paged' => get_query_var('paged') ];
    $loop_for_posts = new WP_Query( $args_for_posts );
    ;?>
    
    
    <div class="post__wrapper">
      <?php while( $loop_for_posts->have_posts() ) : $loop_for_posts->the_post(); ?>
        
        <article class="bCard">
          <figure class="bCard__figure">
            <img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'bCard') ;?>" width="364" height="190" alt="">
          </figure>
          <div class="bCard__wrapper">
            <h3 role="heading" aria-level="3" class="bCard__title"><?= str_limit( get_the_title(), 50 ) ?></h3>
            <p class="bCard__excerp">
              
              <?php
              // If the page has Flexible Content:
              if ( get_field('article') ) :
                $rows = get_field('article');
                // Get the first instance of the Body Copy field
                foreach( array_slice($rows, 0, 1) as $row ) {
                  $excerpt = $row['article-wysiwyg'];
                  echo str_limit( strip_tags( $excerpt ), 200 );
                }
              // Otherwise get the content
              else: ?>
                <?php echo 'Veuillez ajoutez du texte à votre article'; ?>
              <?php endif; ?>
            
            </p>
          </div>
          <div class="bCard__fake">
            <span class="bCard__fake__link"><?php pll_e('Lire l’article'); ?></span>
          </div>
          <a href="<?= the_permalink(); ?>" class="bCard__link">
            <span class="hidden"><?php  pll_e('Lire l’article'); ?> <?= the_title(); ?></span>
          </a>
        </article>
      
      <?php endwhile; ?>
      
      <?php wp_pagenavi(array('query' => $loop_for_posts)); ?>
      
      <?php wp_reset_postdata(); ?>
    
    </div>


</main>

<?php get_footer(); ?>

