<?php
/*
Retrieves the absolute URI for given asset in this theme
*/
function get_theme_asset($src = '') {
  return get_template_directory_uri() . '/public/' . trim($src, '/');
}

/*
Displays the absolute URI for given asset in this theme
*/
function theme_asset($src = '') {
  echo get_theme_asset($src);
}

