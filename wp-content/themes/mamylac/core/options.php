<?php

function wp_register_options()
{
  if( function_exists('acf_add_options_page') ) {
    // add parent
    $parent = acf_add_options_page([
      'page_title' => 'Options du site',
      'menu_title' => 'Options du site',
      'menu_slug' => 'acf-options'
    ]);
    acf_add_options_sub_page([
      'page_title' => 'Général',
      'menu_title' => 'Général',
      'menu_slug' => 'acf-options-general',
      'parent_slug' => $parent['menu_slug']
    ]);
    acf_add_options_sub_page([
      'page_title' => 'Réseaux sociaux',
      'menu_title' => 'Réseaux sociaux',
      'menu_slug' => 'acf-options-social',
      'parent_slug' => $parent['menu_slug']
    ]);
  }
}
