<?php

function wp_register_types()
{
  register_post_type('slider', [
    'label'         => 'Slider',
    'labels'        => [
      'all_items' => 'Tous les slides',
      'singular_name' => 'slider',
      'add_new_item'  => 'Ajouter un nouveau slide',
      'add_new'  => 'Ajouter un nouveau slide',
    ],
    'description'   => 'Permet d’ajouter des slides à la page d’accueil',
    'public'        => true,
    'menu_position' => 21,
    'menu_icon'     => 'dashicons-slides',
    'supports' => ['title', 'thumbnail']
  ]);
  
  register_post_type('photos', [
    'label'         => 'Photos',
    'labels'        => [
      'all_items' => 'Tous les albums photos',
      'singular_name' => 'photo',
      'add_new_item'  => 'Ajouter un nouvel album',
      'add_new'  => 'Ajouter un nouvel album',
    ],
    'description'   => 'Permet d’ajouter des albums de photos dans la page Medias/Photos',
    'public'        => true,
    'menu_position' => 22,
    'menu_icon'     => 'dashicons-format-gallery',
    'supports' => ['title', 'thumbnail']
  ]);
  
  register_post_type('press', [
    'label'         => 'Revue de presse',
    'labels'        => [
      'all_items' => 'Toutes les revues de presse',
      'singular_name' => 'revue de presse',
      'add_new_item'  => 'Ajouter une nouvelle revue de presse',
      'add_new'  => 'Ajouter une nouvelle revue de presse',
    ],
    'description'   => 'Permet d’ajouter des revues de presse dans la page Medias/Revue de presse',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 23,
    'menu_icon'     => 'dashicons-media-document',
    'supports' => ['title']
  ]);
  
  register_post_type('conseils', [
    'label'         => 'Conseils',
    'labels'        => [
      'all_items' => 'Tous les conseils',
      'singular_name' => 'conseil',
      'add_new_item'  => 'Ajouter un nouveau conseil',
      'add_new'  => 'Ajouter un nouveau conseil',
    ],
    'description'   => 'Permet d’ajouter des conseils',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 24,
    'menu_icon'     => 'dashicons-megaphone',
    'supports' => ['title', 'thumbnail']
  ]);
  
  register_post_type('produits', [
    'label'         => 'Produits',
    'labels'        => [
      'all_items' => 'Tous les produits',
      'singular_name' => 'produit',
      'add_new_item'  => 'Ajouter un produit',
      'add_new'  => 'Ajouter un produit',
    ],
    'description'   => 'Permet d’ajouter des produits',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 24,
    'menu_icon'     => 'dashicons-products',
    'taxonomies' => array( 'category'),
    'supports' => ['title', 'thumbnail'],
  ]);
  
  register_post_type('machines', [
    'label'         => 'Machines',
    'labels'        => [
      'all_items' => 'Toutes les machines',
      'singular_name' => 'machine',
      'add_new_item'  => 'Ajouter une machine',
      'add_new'  => 'Ajouter une machine',
    ],
    'description'   => 'Permet d’ajouter des machines',
    'public'        => true,
    'show_in_menu' => true,
    'menu_position' => 24,
    'menu_icon'     => 'dashicons-desktop',
    'supports' => ['title', 'thumbnail'],
  ]);
}
