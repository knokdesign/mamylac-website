<?php

add_theme_support( 'post-thumbnails' );
add_image_size( 'hSlider', 1440 );
add_image_size( 'bCard', 364, 190, true );
add_image_size( 'sHeader', 1440, 255, true );
add_image_size( 'wysiwygImage', 946);
add_image_size( 'albumCover', 267, 211, true);
add_image_size( 'contactPhoto', 345, 305, true);
add_image_size( 'productLogo', 160);
add_image_size( 'productConso', 269);
add_image_size( 'productImage', 388);
add_image_size( 'productImage2', 287, 347, true);
add_image_size( 'aboutImage', 326, 200, true);
