<?php
/*
 * Set H1
 */
function setH1()
{
  if (is_page('accueil')){
    echo get_bloginfo()  . ' - ' . get_bloginfo('description');
  } else {
    echo the_title() . ' - ' . get_bloginfo();
  }
}
