<?php

/*
 * Generates a custom menu array
 */
function b_get_menu_id( $location )
{
  $a = get_nav_menu_locations();
  if (isset($a[$location])) return $a[$location];
  return false;
}
function b_get_menu_items( $location )
{
  $navItems = [];
  foreach (wp_get_nav_menu_items( b_get_menu_id($location) ) as $obj) {
    // Si vous avoir un contrôle sur les liens affichés, c'est ici. (Par exemple: mettre $item->isCurrent à true|false)
    $item = new stdClass();
    $item->url = $obj->url;
    $item->label = $obj->title;
    $item->icon = $obj->classes[0];
    $item->id = $obj->object_id;
    array_push($navItems, $item);
  }
  return $navItems;
}


/*
  Dropdown
*/

/*
 * Get navigation links (objects) for given location
*/
function b_get_nav_items($location) {
  $id = b_get_menu_id($location);
  $nav = [];
  $children = [];
  if(!$id) {
    return $nav;
  }
  foreach(wp_get_nav_menu_items($id) as $object) {
    $item = new stdClass();
    $item->url = $object->url;
    $item->label = $object->title;
    $item->id = $object->object_id;
    $item->icon = $object->classes[0];
    $item->parent = intval($object->menu_item_parent);
    $item->children = [];
    if($item->parent){
      $children[] = $item;
    } else {
      $nav[$object->ID] = $item;
    }
  }
  foreach($children as $item) {
    $nav[$item->parent]->children[] = $item;
  }
  return $nav;
}


// Create menu active
add_filter('menu__item' , 'active' , 10 , 2);
function special_nav_class($classes, $item){
  if( in_array('current-menu-item', $classes) ){
    $classes[] = 'active';
  }
  return $classes;
}
/*
 *    Generates a languages menu
 *    Based on Polylang (plugin)
 */
function b_get_languages()
{
  $langItems = [];
  $rawItems = pll_the_languages( [
    'echo' => false,
    'hide_if_empty' => false,
    'hide_if_no_translation' => false,
    'raw' => true
  ] );
  foreach ($rawItems as $raw) {
    // Si vous souhaitez faire des manipulations sur le format des données, c'est ici. (Par exemple: changer les codes de langues de "es" à "ESP" pour des besoins en CSS)
    $item = new stdClass();
    $item->label = $raw['name'];
    $item->url = $raw['url'];
    $item->code = $raw['slug'];
    array_push($langItems, $item);
  }
  return $langItems;
}
