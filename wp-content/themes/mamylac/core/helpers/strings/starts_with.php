<?php

/*
 * Determine if a given string starts with a given substring.
 */

function starts_with($haystack, $needles)
{
  foreach ((array) $needles as $needle) {
    if ($needle !== '' && substr($haystack, 0, strlen($needle)) === (string) $needle) {
      return true;
    }
  }
  return false;
}
