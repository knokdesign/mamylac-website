<?php

/*
Template Name: Vidéos
*/

get_template_part('parts/meta');; ?>

<body class="album" data-page="album">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?php the_permalink(354); ?>"><?php pll_e('Retour à la liste des médias'); ?></a>
		</div>
	</div>
  <div class="containerGlobal about__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="about__title title24Bold"><?php the_title(); ?></h2>
  
      <div class="lightGallery" id="galleryPost">
      <?php if( have_rows('videos') ): while ( have_rows('videos') ) : the_row(); ?>
        <?php $image = get_sub_field('vignette'); ?>
          <a class="lightGallery__link" href="<?= the_sub_field('video'); ?>&rel=0">
            <div class="lightGallery__wrapper">
              <img class="lightGallery__img" src="<?= $image['sizes']['albumCover'];?>" width="267" height="211" alt="">
              <svg width="30" height="36" xmlns="http://www.w3.org/2000/svg">
                <path d="M30 18c0 .9-1 1.5-1 1.5l-25.6 16c-1.9 1.2-3.4.3-3.4-2v-31C0 .3 1.5-.6 3.4.6l25.7 16s.9.6.9 1.5z" fill="#FFF"/>
              </svg>
            </div>
            <strong class="title19"><?= the_sub_field('name'); ?></strong>
          </a>
      <?php endwhile; ?>
      <?php endif; ?>
      </div>
      

</main>

<?php get_footer(); ?>

