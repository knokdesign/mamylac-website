w.menu = {
  oConf: {},

  init: function () {
    this.initNavMain();
    this.responsiveMenu();
  },

  // getElements: function () {
  // },
  // setEvents: function () {

  // },

  // EVENTS

  // FUNCTIONS
  initNavMain: function () {
    $('.nav-menu__wrapper').accessibleMegaMenu({
      uuidPrefix: "accessible-megamenu",

      /* css class used to define the megamenu styling */
      menuClass: "nav-menu",

      /* css class for a top-level navigation item in the megamenu */
      topNavItemClass: "nav-item",

      /* css class for a megamenu panel */
      panelClass: "sub-nav",

      /* css class for a group of items within a megamenu panel */
      panelGroupClass: "sub-nav-group",

      /* css class for the hover state */
      hoverClass: "hover",

      /* css class for the focus state */
      focusClass: "focus",

      /* css class for the open state */
      openClass: "open"
    });
  },

  responsiveMenu: function () {
    $('.hamburger').click(function () {
      $(this).toggleClass('is-active');
      $('body').toggleClass('menu-open');
    });

    $('.grey').click(function () {
      $('body').removeClass('menu-open');
      $('.hamburger').removeClass('is-active');
    })
  }
}
