var w = {
  // global vars
  sCurrent: null,
  isLoaded: false,

  // global elements
  $body: null,

  // Navigation responsive

  // pages
  home: {},

  // setup
  init: function () {
    w.getElements();
    w.sCurrent = w.getCurrentPage();
    w.loadPage();
    w.isLoaded = true;
    w.menu.init();
  },
  loadPage: function () {
    if (w.sCurrent) w[w.sCurrent].init();
  },

  // functions
  getElements: function () {
    w.$body = $('body');
  },

  getCurrentPage: function () {
    if (w.$body.data('page') == 'home' ) return 'home';
    if (w.$body.data('page') == 'album' ) return 'album';
    if (w.$body.data('page') == 'contact' ) return 'contact';
    return false;
  }
};

$(window).on('load', w.init);
