w.contact = {
  oConf: {},

  init: function () {

  },

  // getElements: function () {
  initMap: function () {
    var myLatLng = { lat: 50.415442, lng: 6.0011303 }

    var map = new google.maps.Map(document.getElementById('map'), {
      center: { lat: 50.417961, lng: 6.0011303 },
      zoom: 15
    });

    // Create marker and set its position.
    var icon = 'https://mamylac.be/wp-content/themes/mamylac/public/img/map-marker.svg'
    var marker = new google.maps.Marker({
      map: map,
      position: myLatLng,
      title: 'Mamylac',
      icon: icon
    });
    marker.set("id", 'mapmarker');

    google.maps.event.addDomListener(window, "resize", function() {
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });



  }
  // },
  // setEvents: function () {

  // },

  // EVENTS

  // FUNCTIONS
}
