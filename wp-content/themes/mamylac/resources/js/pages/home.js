w.home = {
      oConf: {},

      init: function () {
        this.hSlider();
        this.clickIframe();
      },

      // getElements: function () {
      // },
      // setEvents: function () {

      // },

      // EVENTS

      // FUNCTIONS
      hSlider: function () {
        var slider = $("#hSlider").lightSlider({
          item: 1,
          loop: true,
          slideMove: 1,
          easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
          speed: 600,
          pager: false,
          slideMargin: 0,
          enableDrag: false,
          auto: true,
          pause: 6000,
          pager: true,
          pauseOnHover: true,
          controls: false,
          onSliderLoad: function() {
            $('#hSlider').removeClass('cS-hidden');
            $('#hSlider').addClass('hSlider__anim');
          }
        });

        $('.hSlider__button--left').click(function () {
          slider.goToPrevSlide();
        })

        $('.hSlider__button--right').click(function () {
          slider.goToNextSlide();
        })
      },

      clickIframe: function () {
        var videos  = $(".video-player__wrapper");

        videos.on("click", function(){
          var elm = $(this),
              conts   = elm.contents(),
              le      = conts.length,
              ifr     = null;

          for(var i = 0; i<le; i++){
            if(conts[i].nodeType == 8) ifr = conts[i].textContent;
          }

          elm.addClass("player").html(ifr);
          elm.off("click");
        });
      }
}
