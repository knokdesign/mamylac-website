# Fonts for the project

All fonts used for the project.
The iconfont isn't present because it's generated from the `gulp iconfont` task.

/!\ All font families must be placed in a folder with their names, for example, the font family Catamaran is placed in a folder Catamaran.