<?php
/*
Template Name: Home
*/
get_template_part('parts/meta');
;?>

<body class="home" data-page="home">
	<div class="grey"></div>
	<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
	<?php get_header(); ?>

	<main>
		<section class="hIntro">
			<div class="hIntro__wrapper hIntro__wrapper--left">
				<h2 role="heading" aria-level="2" class="hIntro__title"><?= get_field('home_intro_title') ;?></h2>
				<p class="hIntro__desc">
          <?= get_field('introduction_texte') ;?>
				</p>
				<a href="<?= pll_current_language() == 'fr' ? the_permalink(84) : the_permalink(750);?>" class="hIntro__link">
					<svg width="7" height="13" xmlns="http://www.w3.org/2000/svg">
						<path d="M4.4 6.1L.3 1.6A1 1 0 0 1 .3.3c.3-.4.9-.4 1.3 0l5 5.2c.4.3.4.9 0 1.2l-5 5.2c-.4.4-1 .4-1.3 0a1 1 0 0 1 0-1.3l4.1-4.5z" fill="#91268F"/>
					</svg>
					<?php pll_e('En savoir plus sur Mamylac'); ?>
				</a>
			</div>
			<div class="hIntro__wrapper hIntro__wrapper--right">
				<div class="video-player">
					<div class="video-player__wrapper">
						<div class="video-player__thumb">
							<img src="<?= theme_asset('img/video-img.jpg') ;?>" width="582" height="328" alt="">
							<span class="video-player__info">
								<svg width="32" height="38" xmlns="http://www.w3.org/2000/svg">
									<path d="M32 19c0 1-1 1.6-1 1.6L3.6 37.4c-2 1.3-3.6.4-3.6-2.1V2.7C0 .2 1.6-.7 3.6.6L31 17.4s1 .7 1 1.6z" fill="#FFF"/>
								</svg>
								<?php pll_e('Voir la vidéo'); ?>
							</span>
						</div>
						<!--<iframe width="582" height="328" src="https://www.youtube.com/embed/<?= get_field('home_youtube') ;?>?feature=oembed&autoplay=1&rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen frameborder="0"></iframe>-->
					</div>
				</div>
			</div>
		</section>
		
		<blockquote class="quote quote--home">
			<div class="quote__wrapper">
				<p class="quote__text">
          <?= get_field('home_quote'); ?>
				</p>
			</div>
		</blockquote>
		
		<section class="hBlog">
			<h2 role="heading" aria-level="2" class="hBlog__title hidden"><?php pll_e('Nos derniers articles'); ?></h2>
      <?php
      	$args_for_hBlog = ['post_type' => 'post', 'posts_per_page' => 3];
      	$loop_for_hBlog = new WP_Query( $args_for_hBlog );
      ;?>
      
      <?php while( $loop_for_hBlog->have_posts() ) : $loop_for_hBlog->the_post(); ?>
			<article class="bCard">
				<figure class="bCard__figure">
					<img src="<?= get_the_post_thumbnail_url(get_the_ID(), 'bCard') ;?>" width="364" height="190" alt="">
				</figure>
				<div class="bCard__wrapper">
					<h3 role="heading" aria-level="3" class="bCard__title"><?= str_limit( get_the_title(), 50 ) ?></h3>
					<p class="bCard__excerp">
						
						<?php
						// If the page has Flexible Content:
						if ( get_field('article') ) :
							$rows = get_field('article');
							// Get the first instance of the Body Copy field
							foreach( array_slice($rows, 0, 1) as $row ) {
								$excerpt = $row['article-wysiwyg'];
								echo str_limit( strip_tags( $excerpt ), 200 );
							}
						// Otherwise get the content
						else: ?>
							<?php echo 'Veuillez ajoutez du texte à votre article'; ?>
						<?php endif; ?>
						
					</p>
				</div>
				<div class="bCard__fake">
					<span class="bCard__fake__link"><?php pll_e('Lire l’article'); ?></span>
				</div>
				<a href="<?= the_permalink(); ?>" class="bCard__link">
					<span class="hidden"><?php  pll_e('Lire l’article'); ?> <?= the_title(); ?></span>
				</a>
			</article>
      <?php endwhile; ?>
			<?php wp_reset_postdata(); ?>
		</section>
		
		
		
		
		
	</main>
	
	<?php get_footer(); ?>

