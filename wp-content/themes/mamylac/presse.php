<?php

/*
Template Name: PresseE
*/

get_template_part('parts/meta');; ?>

<body class="press" data-page="press">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?php the_permalink(354); ?>"><?php pll_e('Retour à la liste des médias'); ?></a>
		</div>
	</div>
  <div class="containerGlobal about__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="about__title title24Bold"><?php the_title(); ?></h2>
  
    <?php
    $args_for_press = ['post_type' => 'press'];
    $loop_for_press = new WP_Query( $args_for_press );
    ;?>
		
		
    <div class="lightGallery" id="galleryPost">
      <?php while( $loop_for_press->have_posts() ) : $loop_for_press->the_post(); ?>
        <?php $image = get_field('image'); ?>
				
				<?php
					$link = null;
					if (get_field('q-file') == 'Je veux uploader un fichier' ){
						$link = get_field('fichier');
					} elseif (get_field('q-file') == 'Je veux pointer vers une page web' ){
            $link = get_field('link');
					}
				?>
				
        <a class="lightGallery__link" href="<?= $link ?>" target="_blank">
          <div class="lightGallery__wrapper">
            <img class="lightGallery__img" src="<?= $image['sizes']['albumCover'];?>" width="267" height="211" alt="">
						<?php if (get_field('q-file') == 'Je veux pointer vers une page web') : ?>
						<svg width="30" height="31" xmlns="http://www.w3.org/2000/svg">
							<path d="M11 24.7l-1.5 1.5a3.4 3.4 0 0 1-4.8 0 3.3 3.3 0 0 1 0-4.7l5.6-5.6c1.1-1.1 3.3-2.8 5-1.2a1.9 1.9 0 1 0 2.5-2.7c-2.7-2.6-6.7-2.1-10.1 1.3L2 18.8a7 7 0 0 0 0 10 7 7 0 0 0 10 0l1.5-1.5c.7-.7.7-1.9 0-2.6-.7-.7-1.9-.7-2.6 0zM28 3c-3-2.8-7-3-9.7-.3l-2 1.8A1.9 1.9 0 1 0 19 7.3l2-1.9c1.4-1.4 3.2-.8 4.4.4a3.3 3.3 0 0 1 0 4.7l-6 5.9c-2.7 2.7-4 1.4-4.5.9a1.9 1.9 0 1 0-2.6 2.6 5.9 5.9 0 0 0 4.2 1.9c1.8 0 3.7-1 5.6-2.8l6-5.9a7 7 0 0 0 0-10z" fill="#FFF"/>
						</svg>
            <?php elseif (get_field('q-file') == 'Je veux uploader un fichier') : ?>
							<svg width="30" height="30" xmlns="http://www.w3.org/2000/svg">
								<path d="M13.8 9.3c.2 0 .2 0 0 0 .2-.6.4-1 .4-1.5V5.7l-.2-.2s0 .2-.2.2c-.3 1-.3 2.1 0 3.6zm-5 11.5l-.8.5c-1.2 1-2 2.2-2.2 2.7 1-.2 2-1.2 3-3.2.2 0 .2 0 0 0 .2 0 0 0 0 0zm15.4-2.5c-.2-.1-.9-.6-3.2-.6h-.3v.1a9 9 0 0 0 3.1.9h.5v-.2l-.1-.2zM26.7 0H3.3A3.3 3.3 0 0 0 0 3.3v23.4C0 28.5 1.5 30 3.3 30h23.4c1.8 0 3.3-1.5 3.3-3.3V3.3C30 1.5 28.5 0 26.7 0zm-1.9 19.7c-.3.1-.8.3-1.5.3a12 12 0 0 1-5-1.2c-2.8.4-5 .7-6.6 1.4l-.4.1c-2 3.5-3.6 5.2-5 5.2-.3 0-.5 0-.6-.2l-.9-.5v-.1l-.1-.9c.1-.8 1.1-2.3 3.1-3.5.4-.1.9-.5 1.5-.8l1.7-3c.8-1.7 1.3-3.3 1.8-4.8-.6-2-1-3.2-.3-5.5.2-.7.7-1.4 1.3-1.4h.4c.3 0 .6.2 1 .4 1.1 1.1.6 3.8 0 6v.1a12 12 0 0 0 2.6 4.4l1.5 1 2.2-.2c2 0 3.3.3 3.8 1.2.2.3.2.6.2 1-.2.1-.3.6-.7 1zM14 13.2c-.3 1.1-1 2.5-1.7 4l-1 1.8h.4c2.1-.8 4.1-1.3 5.5-1.5l-.7-.5a17 17 0 0 1-2.5-3.8z" fill="#FFF"/>
							</svg>
						<?php endif; ?>
          </div>
          <strong class="title19"><?= the_title(); ?></strong>
					<span class="media"><?= get_field('name') ;?></span>
        </a>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
		</div>
	</div>


</main>

<?php get_footer(); ?>

