<?php
/*
Template Name: Photos
*/
get_template_part('parts/meta');; ?>

<body class="albums" data-page="albums">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?php the_permalink(354); ?>"><?php pll_e('Retour à la liste des médias'); ?></a>
		</div>
	</div>
  <div class="containerGlobal about__container containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="about__title title24Bold"><?php the_title(); ?></h2>
  
    <?php
    $args_for_albums = ['post_type' => 'photos'];
    $loop_for_albums = new WP_Query( $args_for_albums );
    ;?>
		
		<div class="albums__container">
    <?php while( $loop_for_albums->have_posts() ) : $loop_for_albums->the_post(); ?>
			<a class="albums__link" href="<?= the_permalink(); ?>" title="<?php pll_e('Vers l’album '); the_title(); ?> ">
				<figure class="albums__figure">
					<img src="<?= the_post_thumbnail_url('albumCover'); ?>" width="267" height="217" alt="">
					<figcaption class="albums__figcaption"><?= the_title(); ?></figcaption>
				</figure>
			</a>
    <?php endwhile; ?>
		</div>
    <?php wp_reset_postdata(); ?>
	</div>

</main>

<?php get_footer(); ?>

