<?php


get_template_part('parts/meta');

$subtitle = get_field('produit_subtitle');
$color = null;
$color2 = null;
$logo = null;
$txtAv = get_field('produit_head');
$desc = get_field('produit_desc');
$isAlert = get_field('produit_add_info');
$alert = get_field('produit_info');
$pdf = get_field('produit_pdf');

if (get_field('produit_add_color') == 'Oui' ){ $color = 'color: ' . get_field('produit_color') . ';'; }  else {
	$color = '';
}

if (get_field('produit_add_color') == 'Oui' ){ $color2 = get_field('produit_color'); }  else {
  $color2 = '#0072AD';
}

if (get_field('produit_is_logo') == 'Oui' ){
	$logo = get_field('produit_logo')['sizes']['productLogo'];
}  else {
  $logo = '';
}

?>

<body class="product" data-page="product">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  
  <?php $terms = get_the_terms( $post->ID , 'category'); ?>
	
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?= the_permalink() . $terms[0]->slug ;?>"><?php pll_e('Retour à la gamme'); ?></a>
		</div>
	</div>
	
  <section class="containerGlobal containerGlobalFirst" style="margin-top: 30px;">
		<div class="product__wrapper">
			<div class="product__mid product__mid--left">
				<?php if (get_field('produit_is_logo') == 'Oui' ): ?>
					<h2 role="heading" aria-level="2" class="title24Bold hidden"><?php the_title(); ?></h2>
					<img src="<?= $logo ;?>" width="160" height="31" alt="">
				<?php else: ?>
					<h2 role="heading" aria-level="2" class="title24Bold" style="<?= $color; ?>"><?php the_title(); ?></h2>
				<?php endif; ?>
				
				<?php if (isset($subtitle)): ?>
					<span class="product__subtitle" style="<?= $color; ?>"><?= $subtitle ;?></span>
				<?php endif; ?>
        
        <?php if (isset($txtAv)): ?>
					<span class="product__txtAv"><?= $txtAv ;?></span>
        <?php endif; ?>
        
        <?php if (isset($desc)): ?>
					<div class="product__wysiwyg">
						<?= $desc ;?>
					</div>
        <?php endif; ?>
				
				<?php if ($pdf): ?>
					<div class="product__pdf">
						<svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 459 459">
							<path fill="#D80027" d="M211.65 142.8c2.55 0 2.55 0 0 0 2.55-10.2 5.1-15.3 5.1-22.95v-5.1c2.55-12.75 2.55-22.95 0-25.5V86.7l-2.55-2.55s0 2.55-2.55 2.55c-5.1 15.3-5.1 33.15 0 56.1zm-76.5 175.95c-5.1 2.55-10.2 5.1-12.75 7.65-17.85 15.3-30.6 33.15-33.15 40.8 15.3-2.55 30.6-17.85 45.9-48.45 2.55 0 2.55 0 0 0 2.55 0 0 0 0 0zm234.6-38.25c-2.55-2.55-12.75-10.2-48.45-10.2h-5.1v2.55c17.85 7.65 35.7 12.75 48.45 12.75H372.3v-2.55s-2.55 0-2.55-2.55zM408 0H51C22.95 0 0 22.95 0 51v357c0 28.05 22.95 51 51 51h357c28.05 0 51-22.95 51-51V51c0-28.05-22.95-51-51-51zm-28.05 300.9c-5.1 2.55-12.75 5.1-22.95 5.1-20.4 0-51-5.1-76.5-17.85-43.35 5.1-76.5 10.2-102 20.4-2.55 0-2.55 0-5.1 2.55-30.6 53.55-56.1 79.05-76.5 79.05-5.1 0-7.65 0-10.2-2.55l-12.75-7.65v-2.55c-2.55-5.1-2.55-7.65-2.55-12.75 2.55-12.75 17.85-35.7 48.45-53.55 5.1-2.55 12.75-7.65 22.95-12.75 7.65-12.75 15.3-28.05 25.5-45.9 12.75-25.5 20.4-51 28.05-73.95-10.2-30.6-15.3-48.45-5.1-84.15 2.55-10.2 10.2-20.4 20.4-20.4h5.1c5.1 0 10.2 2.55 15.3 5.1 17.85 17.85 10.2 58.65 0 91.8v2.55c10.2 28.05 25.5 51 40.8 66.3 7.65 5.1 12.75 10.2 22.95 15.3 12.75 0 22.95-2.55 33.15-2.55 30.6 0 51 5.1 58.65 17.85 2.55 5.1 2.55 10.2 2.55 15.3-2.55 2.55-5.1 10.2-10.2 15.3zM214.2 201.45c-5.1 17.85-15.3 38.25-25.5 61.2-5.1 10.2-10.2 17.85-15.3 28.05h5.1c33.15-12.75 63.75-20.4 84.15-22.95-5.1-2.55-7.65-5.1-10.2-7.65-12.75-15.3-28.05-35.7-38.25-58.65z"/>
						</svg>
						<a href="<?= $pdf ;?>" target="_blank" class="product__pdfLink"><?php pll_e('Télécharger la fiche technique'); ?></a>
					</div>
				<?php endif; ?>
				
			</div>
			
			<div class="product__mid product__mid--right">
				<img class="product__img" src="<?= get_field('product_image')['sizes']['productImage'] ;?>" width="388" height="" alt="">
			</div>
		</div>
		
		<div class="product__table2">
			<div class="product__table2__row1">
				<div class="product__table2__subrow">
					<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg">
						<g fill="<?= $color2; ?>">
							<path d="M14.2 14.8c2-1.9 2.7-2.8 2.7-4.2 0-1.5-1-2.7-2.7-2.7-.9 0-1.6.3-2.3.9a.5.5 0 1 0 .7.8c.5-.4 1-.6 1.6-.6 1 0 1.6.6 1.6 1.6s-.6 1.8-2.3 3.5L12 15.4c-.3.3 0 1 .4 1H17a.5.5 0 0 0 0-1.1h-3.1l.4-.5zM8.7 9.7v6.1a.5.5 0 1 0 1.1 0V8.7c0-.5-.5-.7-.8-.5l-1.3.9a.5.5 0 1 0 .6.9l.4-.3zM23 18h-2.8C19 18 18 19 18 20.2v3.3a.5.5 0 0 0 1 0v-3.3c0-.6.6-1.1 1.2-1.1h2.7a.5.5 0 0 0 0-1.1z"/>
							<path d="M18.5.5c0-.3.3-.5.6-.5h2.7C23 0 24 1 24 2.2v16.3c0 .2 0 .3-.2.4L19 24l-.4.1H2.2A2.4 2.4 0 0 1 0 21.8V2.2C0 1 1 0 2.2 0h3.3c.3 0 .5.2.5.5v.8c0 .2-.1.4-.3.5a1.4 1.4 0 0 0 .9 2.5c.5 0 .9-.5 1-1 .1-.6-.2-1.3-.7-1.5a.5.5 0 0 1-.4-.5V.5c0-.3.3-.5.6-.5h4.4c.3 0 .5.2.5.5v.8c0 .2-.1.4-.3.5a1.4 1.4 0 0 0 .9 2.5c.5 0 .9-.5 1-1 .1-.6-.2-1.3-.7-1.5a.5.5 0 0 1-.4-.5V.5c0-.3.3-.5.6-.5h4.4c.3 0 .5.2.5.5v.8c0 .2-.1.4-.3.5a1.4 1.4 0 0 0 .9 2.5c.5 0 .9-.5 1-1 .1-.6-.2-1.3-.7-1.5a.5.5 0 0 1-.4-.5V.5zm1.1 0v.8l-.3-.5a2.7 2.7 0 0 1 .5.3 2.4 2.4 0 0 1-1 4.3A2.5 2.5 0 0 1 17.2.8l-.3.5V.5l.6.6H13l.5-.6v.8l-.3-.5a2.7 2.7 0 0 1 .5.3 2.4 2.4 0 0 1-1 4.3A2.5 2.5 0 0 1 11.2.8l-.3.5V.5l.6.6H7l.5-.6v.8L7.3.8a2.7 2.7 0 0 1 .5.3 2.4 2.4 0 0 1-1 4.3A2.5 2.5 0 0 1 5.2.8l-.3.5V.5l.6.6H2.2A1 1 0 0 0 1 2v19.7c0 .6.5 1.1 1 1.1h16.2l4.6-4.6v-16c0-.6-.5-1.1-1-1.1H19l.5-.6z"/>
						</g>
					</svg>
					<span><?php pll_e('Période'); ?></span>
				</div>
				<div class="product__table2__subrow">
					<svg width="22" height="24" xmlns="http://www.w3.org/2000/svg">
						<g fill="<?= $color2; ?>">
							<path d="M18.8 19.5l1.9-1.8-.7-.7-.7-.7-1.9 1.9-1.8 1.8-.5 1.9 1.9-.5 1.8-1.9zm-1.4 2.8l-2.9.8a.5.5 0 0 1-.6-.6l.8-2.9v-.2l2-2 2.2-2.2c.2-.2.6-.2.8 0l1 1 1 1.1c.2.2.2.5 0 .7l-2.2 2.3-2 1.9-.1.1z"/>
							<path d="M15 20.5l1.6 1.5.7-.8-1.6-1.5zM13.8 2c.2 0 .4.2.5.4l1 3.6c0 .3-.2.6-.5.6H4.6a.5.5 0 0 1-.5-.6l1-3.6c0-.2.3-.4.5-.4h2a2 2 0 0 1 4.1 0h2zm-2.6 1a.5.5 0 0 1-.5-.4V2a1 1 0 1 0-2 0v.6c0 .2-.2.5-.5.5H6l-.7 2.5H14l-.7-2.5h-2.2z"/>
							<path d="M13.3 23H2a1 1 0 0 1-1-1V4.6c0-.6.5-1 1-1h3.1v-1h-3a2 2 0 0 0-2.1 2V22c0 1 1 2 2 2h11.3v-1zm1-19.4h3c.6 0 1 .4 1 1v8.2h1.1V4.6c0-1.1-1-2-2-2h-3.1v1z"/>
							<path d="M3 17.7v-12h2.1V4.5H2.6c-.3 0-.6.2-.6.5v13l.2.3 3.6 3.6.3.2h6.7v-1H6.3l-3.2-3.3zm13.3-5.4h1V5c0-.3-.2-.5-.4-.5h-2.6v1h2v6.7z"/>
							<path d="M5.6 21.4h1V18c0-.3-.2-.5-.5-.5H2.6v1h3v3zm-.5-10.7h3.6v-1H5zm0 2.6h8.7v-1H5zm0 2.5h7.2v-1H5z"/>
						</g>
					</svg>
					<span><?php pll_e('Composition'); ?></span>
				</div>
				<div class="product__table2__subrow">
					<svg width="23" height="24" xmlns="http://www.w3.org/2000/svg">
						<g fill="<?= $color2; ?>">
							<path d="M13.8 2.2c.2 0 .4.1.4.3l1 3.6c.1.3 0 .6-.4.6H4.7a.5.5 0 0 1-.5-.6l1-3.6c0-.2.3-.3.5-.3h2a2 2 0 0 1 4 0h2zm-2.6 1a.5.5 0 0 1-.5-.5v-.5a1 1 0 1 0-2 0v.5c0 .3-.2.5-.5.5H6.1l-.7 2.5H14l-.7-2.5h-2.2z"/>
							<path d="M16.3 22.8H2.2a1 1 0 0 1-1-1V4.7c0-.6.4-1 1-1h3v-1h-3a2 2 0 0 0-2 2v17.1c0 1.1.9 2 2 2h14v-1zm-2-19.1h3c.5 0 1 .4 1 1v8h1v-8a2 2 0 0 0-2-2h-3v1z"/>
							<path d="M3.2 17.6V5.7h2v-1H2.7c-.3 0-.5.2-.5.5v12.6l.1.3L6 21.7l.3.1h6v-1H6.5l-3.2-3.2zm13.1-5.3h1V5.2c0-.3-.2-.5-.5-.5h-2.5v1h2v6.6z"/>
							<path d="M5.7 21.3h1v-3.5c0-.3-.2-.5-.5-.5H2.7v1h3v3zm-.5-10.6h3.5v-1H5.2zm0 2.6h8.6v-1H5.2zm0 2.5h6v-1h-6z"/>
							<path d="M16.3 23.8a6 6 0 1 1 0-12 6 6 0 0 1 0 12zm0-1a5 5 0 1 0 0-10 5 5 0 0 0 0 10z"/>
							<path d="M13.6 17.4a.5.5 0 0 0-.7.7l1.8 1.9 4.4-3.8a.5.5 0 1 0-.6-.8l-3.7 3.2-1.2-1.2z"/>
						</g>
					</svg>
					<span><?php pll_e('Utilisation'); ?></span>
				</div>
			</div>
			<div class="product__table2__row2">
				<div class="product__table2__content" data-title="<?php pll_e('Période'); ?>">
					<ul>
            <?php if (have_rows('produit_periode') ): while ( have_rows('produit_periode') ) : the_row(); ;?>
							<li><?= the_sub_field('item') ;?></li>
            <?php endwhile; endif; ?>
					</ul>
				</div>
				<div class="product__table2__content" data-title="<?php pll_e('Composition'); ?>">
					<ul>
            <?php if (have_rows('produit_composition') ): while ( have_rows('produit_composition') ) : the_row(); ;?>
							<li><?= the_sub_field('item') ;?></li>
            <?php endwhile; endif; ?>
					</ul>
				</div>
				<div class="product__table2__content product__table2__content--last" data-title="<?php pll_e('Utilisation'); ?>">
					<ul>
            <?php if (have_rows('produit_utilisation') ): while ( have_rows('produit_utilisation') ) : the_row(); ;?>
							<li><?= the_sub_field('item') ;?></li>
            <?php endwhile; endif; ?>
					</ul>
				</div>
			</div>
		</div>
  
  
    <?php if ( $isAlert == 'Oui' ): ?>
			<div class="product__alert">
				<svg width="10" height="20" xmlns="http://www.w3.org/2000/svg">
					<path d="M7.4 0c1.4 0 2 1 2 2 0 1.3-1.1 2.5-2.6 2.5-1.3 0-2-.8-2-2 0-1 .9-2.5 2.6-2.5zm-4 20c-1.1 0-2-.7-1.2-3.5l1.2-5.1c.2-.8.3-1.2 0-1.2A8 8 0 0 0 1 11.4l-.5-1A14 14 0 0 1 7.2 7c1 0 1.2 1.3.7 3.2l-1.4 5.4c-.2 1-.1 1.3.1 1.3.3 0 1.4-.4 2.4-1.2l.6.8A11 11 0 0 1 3.3 20z" fill="#0072AD"/>
				</svg>
				<span><?= $alert ;?></span>
			</div>
		<?php endif; ?>
		
		<?php if (get_field('produit_conso') == 'Oui'): ?>
		<section class="product__conso">
			<h2 role="heading" aria-level="2" class="title24Bold"><?php pll_e('L’avis d’un consommateur'); ?></h2>
			<div class="product__conso__wrapper">
				<img src="<?= get_field('produit_conso_photo')['sizes']['productConso'] ;?>" width="269" height="" alt="">
				<div class="product__conso__content">
					<svg width="40" height="32" xmlns="http://www.w3.org/2000/svg">
						<defs>
							<linearGradient x1="0%" y1="50%" y2="50%" id="a">
								<stop stop-color="#00ABD6" offset="0%"/>
								<stop stop-color="#0072AD" offset="100%"/>
							</linearGradient>
						</defs>
						<path d="M2.5 29.3a8 8 0 0 1-2.5-6c0-1.3.2-2.7.7-4C1 17.7 1.9 16 3 13.5L9.7 0H18l-4.4 16.2a8.4 8.4 0 0 1 3.9 7.2c0 2.3-.9 4.3-2.6 5.9a8.8 8.8 0 0 1-6.3 2.4c-2.5 0-4.5-.8-6.2-2.4zm21.3 0a8 8 0 0 1-2.5-6c0-1.3.2-2.7.7-4 .4-1.5 1.2-3.3 2.3-5.7L31 0h8.4L35 16.2a8.4 8.4 0 0 1 3.8 7.2c0 2.3-.8 4.3-2.5 5.9a8.8 8.8 0 0 1-6.3 2.4c-2.5 0-4.6-.8-6.2-2.4z" fill="url(#a)" fill-rule="evenodd"/>
					</svg>
					<p><?= get_field('produit_conso_avis'); ?></p>
					<span><?= get_field('produit_conso_name'); ?></span>
				</div>
			</div>
		</section>
		<?php endif; ?>
		
		
		
		
		
	</section>
    

</main>

<?php get_footer(); ?>

