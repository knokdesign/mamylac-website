<?php

/*
Template Name: Page links
*/

get_template_part('parts/meta');
$textIntro = get_field('links_intro');

?>

<body class="page-link" data-page="page-link">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
  <div class="containerGlobal containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
  
    <?php if (isset($textIntro)): ?>
			<div class="page-link__wysiwyg">
      	<?= $textIntro; ?>
			</div>
    <?php endif; ?>
	
		<h2 role="heading" aria-level="2" class="title22"><?= get_field('links_title') ;?></h2>
    
    <ul class="page-link__list">
      <?php if( have_rows('repeteur') ): while ( have_rows('repeteur') ) : the_row(); ?>
      <li class="page-link__item">
        <a href="<?= get_sub_field('link')['url']; ?>" class="page-link__link">
          <svg width="7" height="13" xmlns="http://www.w3.org/2000/svg">
            <path d="M4.4 6.1L.3 1.6A1 1 0 0 1 .3.3c.3-.4.9-.4 1.3 0l5 5.2c.4.3.4.9 0 1.2l-5 5.2c-.4.4-1 .4-1.3 0a1 1 0 0 1 0-1.3l4.1-4.5z" fill="#91268F"/>
          </svg>
          <span><?= get_sub_field('link')['title']; ?></span>
        </a>
      </li>
      
      <?php endwhile; endif; ?>
      
    </ul>
    
  </div>


</main>

<?php get_footer(); ?>

