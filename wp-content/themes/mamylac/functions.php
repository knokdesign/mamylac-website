<?php


// Disable administration bar for dev
show_admin_bar( false );



@include('core/theme_asset.php');

// Add support for thumbnails
@include('core/thumbnails.php');

// Add SEO functions
@include('core/seo.php');

// Create register_types
add_action('init', 'wp_register_types');
@include('core/register_post_type.php');

// Create page options
add_action('init', 'wp_register_options');
@include('core/options.php');

// Add menu support
/*
 * NAVIGTION
 */

register_nav_menu('main-nav', 'La navigation principale du site.');
register_nav_menu('footer-nav', 'La navigation du footer.');
@include('core/menu.php');



// Include helpers
foreach ( glob( get_template_directory() . '/core/helpers/*/*.php' ) as $filename )
{
  include $filename;
}

// Translate invisible reCaptcha with polylang

function wptricks24_recaptcha_scripts()
{
    wp_deregister_script('google-recaptcha');

    $url = 'https://www.google.com/recaptcha/api.js';
    $url = add_query_arg(array(
        'onload' => 'recaptchaCallback',
        'render' => 'explicit',
        'hl' => pll_current_language('slug')), $url);
    wp_register_script('google-recaptcha', $url, array(), '2.0', true);
}

add_action('wpcf7_enqueue_scripts', 'wptricks24_recaptcha_scripts', 11);

