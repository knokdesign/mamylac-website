<?php

/*
Template Name: Actualités
*/

get_template_part('parts/meta');; ?>

<body class="single" data-page="single">
<h1 class="hidden" role="heading" aria-level="1"><?php setH1(); ?></h1>
<?php get_header(); ?>

<main>
	<div class="breadcrumb containerGlobal">
		<div class="breadcrumb__link">
			<svg width="5" height="9" xmlns="http://www.w3.org/2000/svg">
				<path d="M3.2 4.5l-3-3.3a.7.7 0 0 1 0-1c.3-.3.7-.3 1 0L4.7 4c.3.3.3.7 0 1L1.1 8.8c-.2.3-.6.3-1 0a.7.7 0 0 1 0-1l3.1-3.3z" fill="#1a254f"></path>
			</svg>
			<a href="<?= pll_current_language() == 'fr' ? the_permalink(242) : the_permalink(726);?>"><?php pll_e('Retour à la liste des actualités'); ?></a>
		</div>
	</div>
  <div class="containerGlobal single-post containerGlobalFirst">
    <h2 role="heading" aria-level="2" class="title24Bold"><?php the_title(); ?></h2>
		
		<span class="article-published"><?php pll_e('Publié le '); ?> <?= get_the_date(); ?> </span>
  
  
    <?php while (the_flexible_field('article')): ?>
    
      <?php if (get_row_layout() == 'Ajoutez du texte'): ?>
				
				<div class="article__txt">
					<?php echo the_sub_field('article-wysiwyg'); ?>
				</div>
				
      <?php endif; ?>
    
    
			<?php if (get_row_layout() == 'Ajoutez une image'): ?>
				<div class="article__img">
					<?php
					$img        = get_sub_field('article-image');
					$img_thumb  = 'wysiwygImage';
					$img_width  = $img_thumb . '-width';
					$img_height = $img_thumb . '-height';
					?>
					
					<img src="<?= $img['sizes'][$img_thumb]; ?>" width="<?= $img['width']; ?>" height="<?= $img['height']; ?>" alt="<?= $img['alt']; ?>">
					
				</div>
			<?php endif; ?>
		
  
    <?php endwhile; ?>
		
	</div>
    
    


</main>

<?php get_footer(); ?>

